## Build Systems a la Carte

This is a re-implementation of (at least parts of) the paper "Build Systems a la Carte" in Scala 3.

You can read the original paper (whose implementation uses Haskell) [here](https://www.microsoft.com/en-us/research/uploads/prod/2018/03/build-systems.pdf).
