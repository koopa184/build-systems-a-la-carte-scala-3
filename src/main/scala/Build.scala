import cats.Applicative
import cats.data.State

/**
 * A task that describes how to build a single dependency.
 *
 * @param run A function that describes how to build the dependency, potentially in terms of other dependencies.
 *
 * @tparam C The type of constraint for the effect that the task can use (e.g. Applicative, Monad, etc.)
 * @tparam K The type of the "keys" for things that we want to build
 * @tparam V The output type for the built artifacts
 */
case class Task[C[_[_]], K, V](
    /**
     * The C[F] parameter is an object representing the instance of the typeclass "constraint" that we've put
     * upon the class.
     *
     * The (K => F[V]) is a callback that (effectfully) fetches the final build value for a task with a key of K.
     * Note that, since the `run` method has to be polymorphic over F, it can't know anything about the F that it's
     * working with besides the capabilities of the typeclass instance we pass in; the only way we can interact with
     * the output values of other tasks is through using things like `ap` (in the case of the constraint being Applicative)
     * or `flatMap` (in the case of the constraint being `Monad`) or other such methods.
     */
    run: [F[_]] => (C[F]) => (K => F[V]) => F[V]
)

/**
 * Represents a collection of tasks that all have the same constraint/key/value type.
 *
 * For any tasks that are only inputs to other tasks but don't require any build definitions themselves,
 * we'll expect them to just be present in the input store instead, and return None.
 *
 * For tasks that have actual build definitions, we'll return Some(...) with a description
 * of what dependencies the task has.
 */
type Tasks[C[_[_]], K, V] = K => Option[Task[C, K, V]]

case class Store[I, K, V](
    info: I,
    valuesLookup: K => V,
) {
    def getValue(k: K): V = valuesLookup(k)
    def putValue(k: K, v: V): Store[I, K, V] = Store(
      info,
      (lookupKey: K) => if (lookupKey == k) v else valuesLookup(lookupKey)
    )
}

object Store {
    def empty[I, K, V](i: I) = Store(i, Map.empty)
}

/**
 * A Build is defined as a function, which takes:
 * - A Tasks object, which describes how we can build various keys
 * - A key of type K that we would like to figure out the final value of
 * - An initial store, which may contain values for only input keys but not others
 * and returns a final store where the key we've built has been updated to have its final value.
 */
type Build[C[_[_]], I, K, V] = Tasks[C, K, V] => K => Store[I, K, V] => Store[I, K, V]

def busy[K, V]: Build[Applicative, Unit, K, V] = (tasks: Tasks[Applicative, K, V]) => (k: K) => (store: Store[Unit, K, V]) => {
    def fetch: K => State[Store[Unit, K, V], V] = (key: K) => {
        tasks(key) match {
            case None => State.inspect(_.getValue(key))
            case Some(task) => for {
                taskValue <- task.run(Applicative[[A] =>> State[Store[Unit, K, V], A]])(fetch)
                _ <- State.modify[Store[Unit, K, V]](_.putValue(key, taskValue))
            } yield taskValue
        }
    }
    fetch(k).runS(store).value
}