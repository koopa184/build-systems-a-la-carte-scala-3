import cats.Applicative

val sprsh1: Tasks[Applicative, String, Int] = (k: String) => PartialFunction.condOpt(k) {
    case "B1" => Task[Applicative, String, Int](
        run = [F[_]] => (a: Applicative[F]) => (fetch: String => F[Int]) => {
            a.ap2(a.pure[(Int, Int) => Int](_ + _))(fetch("A1"), fetch("A2"))
        }
    )
    case "B2" => Task[Applicative, String, Int](
        run = [F[_]] => (a: Applicative[F]) => (fetch: String => F[Int]) => {
            a.ap(a.pure[Int => Int](_ * 2))(fetch("B1"))
        }
    )
}


@main def hello: Unit =
  val store = Store[Unit, String, Int]((), (k: String) => if (k == "A1") 10 else 20)
  val updatedStore = busy[String, Int](sprsh1)("B2")(store)
  println(updatedStore.getValue("B2"))
  println(updatedStore.getValue("B1"))
  println(updatedStore.getValue("A1"))
  println(updatedStore.getValue("A2"))
