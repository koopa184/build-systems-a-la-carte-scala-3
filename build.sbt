val scala3Version = "3.1.3"

lazy val root = project
  .in(file("."))
  .settings(
    name := "buildalacarte",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies += "org.typelevel" %% "cats-effect" % "3.3.13"
  )
